var elements = document.getElementsByTagName('*');

for (var i = 0; i < elements.length; i++) {
	var element = elements[i];

	for (var j = 0; j < element.childNodes.length; j++) {
		var node = element.childNodes[j];

		if (node.nodeType === 3) {
			var text = node.nodeValue;
			
			var textPairs = [
				// blockchain -> big shared spreadsheet
				['Blockchains', 'Big shared spreadsheets'],
				['blockchains', 'big shared spreadsheets'],
				['A blockchain', 'A big shared spreadsheet'],
				['a blockchain', 'a big shared spreadsheet'],
				['blockchain', 'big shared spreadsheet'],
				['Blockchain', 'Big shared spreadsheet'],
				
				// artificial intelligence -> complicated statistics models
				['Artificial Intelligence', 'Complicated statistics models'],
				['Artificial intelligence', 'Complicated statistics models'],
				['artificial intelligence', 'complicated statistics models'],
				
				// the cloud -> a computer that isn't yours
				['The cloud', 'A computer that isn\'t yours'],
				['the cloud', 'a computer that isn\'t yours'],
				['Clouds', 'Computers that aren\'t yours'],
				['clouds', 'computers that aren\'t yours'],
				['cloud', 'computer that isn\'t yours'],
				
				// cloud computing -> asking someone else to run your program for you
				['cloud computing', 'asking someone else to run your programs for you'],
				['Cloud computing', 'Asking someone else to run your programs for you'],
				
				// DRM -> breaking stuff so other people can't use it
				['DRM', 'breaking stuff so other people can\'t use it'],
				['digital rights management', 'breaking stuff so other people can\'t use it'],
				['Digital rights management', 'Breaking stuff so other people can\'t use it'],
				['digital rights management', 'breaking stuff so other people can\'t use it']
			];
			
			var replacedText = text;
			
			for (var k = 0; k < textPairs.length; k++) {
				var textPair = textPairs[k];
				replacedText = replacedText.replace(new RegExp(textPair[0],'g'), textPair[1]);
			}
			
 			if (replacedText !== text) {
 				element.replaceChild(document.createTextNode(replacedText), node);
 			}
		}
	}
}
