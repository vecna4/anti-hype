# Anti-Hype

This is a browser extension for reducing hyped-up tech buzzwords to roughly equivalent (though much less exciting) descriptions.

I don't recommend actually running this full-time, as it may cause confusion, but it is fun to run for a bit.

Hopefully this extension can bring you some entertainment, as well as providing a bit of educational value.

This was inspired by Cynthia Bailey Lee's [blockchain-to-spreadsheet](https://github.com/cynthiablee/blockchain-to-spreadsheet) extension.

## List of replacements

* blockchain -> big shared spreadsheet
* artificial intelligence -> statistics models
* the cloud -> a computer that isn't yours
* cloud computing -> asking someone else to run your program for you
* DRM -> breaking stuff so other people can't use it

## Notes

* Replacements come in a few forms, to adapt for grammar.
* Artificial intelligence is too broad a term to easily reduce to a short phrase. That said, most of the time when news stories mention AI, they more or less just mean algorithms that statistically predict future outcomes based on past outcomes.

## Contributing

If you have a buzzword you want to contribute an alternative phrase for, feel free to submit a pull request (or just open an issue).

## Credits and Thanks

Big thanks to Cynthia Bailey Lee for making the [blockchain-to-spreadsheet](https://github.com/cynthiablee/blockchain-to-spreadsheet) extension that brought me a lot of joy and inspired this one!

Thanks to Twitter user @mims (Christopher Mims) for who made [the suggestion](https://twitter.com/mims/status/968967786130300928) that led to [blockchain-to-spreadsheet](https://github.com/cynthiablee/blockchain-to-spreadsheet).

Thanks to Tom Maxwell for the [tutorial and template code](https://9to5google.com/2015/06/14/how-to-make-a-chrome-extensions/).
